package koschei.models;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Duck5 {
    private final Egg6 egg;

    public Duck5(@Qualifier("getEgg6") Egg6 egg){this.egg=egg;}

    @Override
    public String toString() {
        return ", v utke yaico " + egg.toString();
    }
}